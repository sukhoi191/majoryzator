import { Translator } from '../__modules__/Translator';
import { SplittedTextPart } from '../__modules__/SplittedTextPart';

var fs = require("fs");

export class TestTranslationBase {
    constructor(translationsPath) {
        let translationsStr = fs.readFileSync(translationsPath, "utf8");
        this.translator = new Translator(JSON.parse(translationsStr).translations);
    }

    translateSingleWord(str) {
        return this.translator.translate([new SplittedTextPart(str, ' ')])[0];
    }

    testTranslation(word, expectedResult) {
        expect(this.translateSingleWord(word)).toStrictEqual(new SplittedTextPart(expectedResult, ' '));
        expect(this.translateSingleWord(word.toLowerCase())).toStrictEqual(new SplittedTextPart(expectedResult.toLowerCase(), ' '));
        expect(this.translateSingleWord(word.toUpperCase())).toStrictEqual(new SplittedTextPart(expectedResult.toUpperCase(), ' '));
    }
}