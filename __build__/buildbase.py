import datetime
import os
import json


class Builder:
    def __init__(self, buildType):
        self.modulesPath = '__modules__'
        self.tamperMonkeyPath = '__tampermonkey__'
        self.buildType = buildType

    # Wypisuje informację z datą i godziną.
    def info(self, str):
        print("[", datetime.datetime.today(), "]", str)

    # Tworzy jeden plik JS z modułów.
    def mergeModulesIntoOneFile(self, mergeOrderPath, mergeFilePath):
        self.info("Łączenie modułów w jeden plik JS.")

        with open(mergeFilePath, "wt", encoding="utf8") as mergeFile:
            with open(mergeOrderPath, "rt", encoding="utf8") as orderFile:
                orderJsonData = json.load(orderFile)
                for orderElement in orderJsonData["MergeOrder"]:
                    self.mergeModule(orderElement, mergeFile)

    def mergeModule(self, orderElement, mergeFile):
        if ("buildType" in orderElement and orderElement["buildType"] != self.buildType):
            return

        self.info("Dopisywanie " + orderElement["fileName"] + "...")

        if (orderElement["type"] == "Code"):
            pathToUse = self.modulesPath
        elif (orderElement["type"] == "TamperMonkey"):
            pathToUse = self.tamperMonkeyPath
        else:
            raise 'Unknown or undefined element type (' + \
                orderElement["type"] + ').'

        fullPath = os.path.join(pathToUse, orderElement["fileName"])

        with open(os.path.join(fullPath), "rt", encoding="utf8") as sourceFile:
            mergeFile.write("// Merged from " + fullPath + "\n")
            for line in sourceFile:
                if (orderElement["type"] == "Code"):
                    self.appendCodeLine(mergeFile, line)
                elif (orderElement["type"] == "TamperMonkey"):
                    self.appendTamperMonkeyLine(mergeFile, line)
                else:
                    raise 'Unknown or undefined element type (' + \
                        orderElement["type"] + ').'

            mergeFile.write("\n\n")

    def appendCodeLine(self, file, line):
        if (line.startswith("import") or "require" in line):
            self.info("- Pomijanie: " + line[:-1])
            return

        newLine = line.replace('export ', '')
        file.write(newLine)

    def appendTamperMonkeyLine(self, file, line):
        file.write(line)
