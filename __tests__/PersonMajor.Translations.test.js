import { TestTranslationBase } from '../__testutils__/TestTranslationsBase';


test('Should translate correctly', () => {
    let translationBase = new TestTranslationBase("__data__/Major_Translations.json");

    translationBase.testTranslation("ą", "q");
    translationBase.testTranslation("zrobią", "zrobiq");

    translationBase.testTranslation("bezczelna", "bszczelna");
    translationBase.testTranslation("bezczelny", "bszczelny");

    translationBase.testTranslation("ę", "em");
    translationBase.testTranslation("zrobię", "zrobiem");
    translationBase.testTranslation("więc", "więc");

    translationBase.testTranslation("grill", "gryl");
    translationBase.testTranslation("grilla", "gryla");

    translationBase.testTranslation("jajecznica", "jajećńca");

    translationBase.testTranslation("jedzenie", "jedzeńe");

    translationBase.testTranslation("kebab", "kabab");
    translationBase.testTranslation("kebabem", "kababem");

    translationBase.testTranslation("Krzysiek", "Ksiek");
    translationBase.testTranslation("Krzysztof", "Ksztof");
    translationBase.testTranslation("Krzysztofem", "Ksztofem");
    translationBase.testTranslation("Krzyśka", "Kśka");
    translationBase.testTranslation("Krzyśkiem", "Kśkiem");

    translationBase.testTranslation("nie", "ńe");
    translationBase.testTranslation("nie wiem", "ńe wiem");
    translationBase.testTranslation("zrobienie", "zrobienie");

    translationBase.testTranslation("obiad", "obiat");
    translationBase.testTranslation("obiadem", "obiatem");

    translationBase.testTranslation("piątek", "piotek");

    translationBase.testTranslation("pizza", "pica");
    translationBase.testTranslation("pizzy", "picy");

    translationBase.testTranslation("policji", "policii");
    translationBase.testTranslation("policja", "policia");
    translationBase.testTranslation("policjant", "policiant");

    translationBase.testTranslation("przyjazd", "przyjast");
    translationBase.testTranslation("zdał", "zdał");
    translationBase.testTranslation("mazda", "mazda");

    translationBase.testTranslation("pysznym", "psznym");
    translationBase.testTranslation("pysznie", "psznie");
    translationBase.testTranslation("pyszny", "pszny");

    translationBase.testTranslation("rz", "ż");
    translationBase.testTranslation("lekarz", "lekaż");
    translationBase.testTranslation("morze", "morze");

    translationBase.testTranslation("nagrywać", "nagrwać");
    translationBase.testTranslation("nagrywa", "nagrwa");
    translationBase.testTranslation("nagrywają", "nagrwajq");

    translationBase.testTranslation("ranny", "rany");
    translationBase.testTranslation("wanna", "wana");

    translationBase.testTranslation("chód", "chut");
    translationBase.testTranslation("samochód", "samochut");

    translationBase.testTranslation("curry", "cury");

    translationBase.testTranslation("sanepid", "sanepik");
    translationBase.testTranslation("sanepidu", "sanepiku");

    translationBase.testTranslation("słyszeć", "słszeć");
    translationBase.testTranslation("słyszysz", "słszysz");
    translationBase.testTranslation("usłyszeć", "usłszeć");

    translationBase.testTranslation("śmietana", "śmiana");
    translationBase.testTranslation("śmietany", "śmiany");
    translationBase.testTranslation("śmietanki", "śmianki");

    translationBase.testTranslation("śniadanie", "śniedańe");
    translationBase.testTranslation("śniadaniowy", "śniedańowy");

    translationBase.testTranslation("spożywa", "spożwa");
    translationBase.testTranslation("spożywać", "spożwać");

    translationBase.testTranslation("też", "tesz");

    translationBase.testTranslation("wreszcie", "wreście");

    translationBase.testTranslation("wspaniała", "spańała");
    translationBase.testTranslation("wspaniały", "spańały");

    translationBase.testTranslation("wyjaśni", "wjaśń");
    translationBase.testTranslation("wyjaśnić", "wjaśńć");
    translationBase.testTranslation("wyjaśnimy", "wjaśńmy");

    translationBase.testTranslation("wypowiadać", "wpowiadać");
    translationBase.testTranslation("wysyłać", "wsyłać");

    translationBase.testTranslation("spodni", "nachów");
    translationBase.testTranslation("spodnie", "nachy");
    translationBase.testTranslation("spodniach", "nachach");

    translationBase.testTranslation("ji", "i");
    translationBase.testTranslation("ja", "ia");
    translationBase.testTranslation("publikacja", "publikacia");
    translationBase.testTranslation("publikacji", "publikaci");
});