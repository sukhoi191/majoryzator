'use strict';

import { PersonKonon } from '../__modules__/PersonKonon';

test('Should return correct welcome', () => {
    expect(new PersonKonon().getCorrectWelcomeForTime(new Date(2000, 1, 1, 0, 0, 0))).toBe("Dobry wieczór państwu");
    expect(new PersonKonon().getCorrectWelcomeForTime(new Date(2000, 1, 1, 5, 0, 0))).toBe("Dzień dobry państwu");
});