'use strict';

import { REGEX_LETTERS } from '../__modules__/Configuration.Functional';
import { Person } from '../__modules__/Person';

test('Should return correct weekday', () => {
    expect(new Person().getDayOfWeek(new Date(2019, 9, 13))).toBe("niedzielę");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 14))).toBe("poniedziałek");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 15))).toBe("wtorek");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 16))).toBe("środę");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 17))).toBe("czwartek");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 18))).toBe("piątek");
    expect(new Person().getDayOfWeek(new Date(2019, 9, 19))).toBe("sobotę");
});

test('Should return correct date string', () => {
    expect(new Person().getDateAsString(new Date(2019, 9, 13))).toBe("13 października");
    expect(new Person().getDateAsString(new Date(2000, 10, 1))).toBe("1 listopada");
});

test('Should return correct time string', () => {
    expect(new Person().getTimeAsString(new Date(2000, 1, 1, 0, 0, 0))).toBe("00:00");
    expect(new Person().getTimeAsString(new Date(2000, 1, 1, 12, 0, 0))).toBe("12:00");
    expect(new Person().getTimeAsString(new Date(2000, 1, 1, 12, 30, 30))).toBe("12:30");
    expect(new Person().getTimeAsString(new Date(2000, 1, 1, 24, 0, 0))).toBe("00:00");
});

test('Should return correct welcome', () => {
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 0, 0, 0))).toBe("dobry wieczór");
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 4, 0, 0))).toBe("dobry wieczór");
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 4, 59, 59))).toBe("dobry wieczór");
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 5, 0, 0))).toBe("dzień dobry");
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 18, 59, 59))).toBe("dzień dobry");
    expect(new Person().getCorrectWelcomeForTime(new Date(2000, 1, 1, 19, 0, 0))).toBe("dobry wieczór");
});

test('Should swap placeholders', () => {
    expect(new Person().swapPlaceholders(['X {PrzywitaniePoraDnia} Y'])).toEqual(expect.arrayContaining([expect.stringMatching(/X (dzień dobry|dobry wieczór) Y/)]));
    expect(new Person().swapPlaceholders(['X {DzienTygodnia} Y'])).toEqual(expect.arrayContaining([expect.stringMatching(/X (poniedziałek|wtorek|środę|czwartek|piątek|sobotę|niedzielę) Y/)]));
    expect(new Person().swapPlaceholders(['X {Data} Y'])).toEqual(expect.arrayContaining([expect.stringMatching(new RegExp("X \\d+ [" + REGEX_LETTERS + "]+ Y"))]));
    expect(new Person().swapPlaceholders(['X {Godzina} Y'])).toEqual(expect.arrayContaining([expect.stringMatching(new RegExp("X \\d{2}:\\d{2} Y"))]));
});