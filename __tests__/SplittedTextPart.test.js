'use strict';

import { SplittedTextPart } from '../__modules__/SplittedTextPart';

test('Should return joined text', () => {
    expect(new SplittedTextPart("abc", null).getJoined()).toBe("abc");
    expect(new SplittedTextPart("abc", "").getJoined()).toBe("abc");
    expect(new SplittedTextPart("abc", " ").getJoined()).toBe("abc ");
    expect(new SplittedTextPart("abc", ",").getJoined()).toBe("abc,");
    expect(new SplittedTextPart("abc", ", ").getJoined()).toBe("abc, ");
});

test('Should remove splitter', () => {
    let part = new SplittedTextPart("abc", " ");
    expect(part.getSplitter()).toBe(" ");

    part.removeSplitter();
    expect(part.getSplitter()).toBeNull();
});