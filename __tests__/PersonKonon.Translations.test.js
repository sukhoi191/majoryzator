import { TestTranslationBase } from '../__testutils__/TestTranslationsBase';

test('Should translate correctly', () => {
    let translationBase = new TestTranslationBase("__data__/Konon_Translations.json");

    translationBase.testTranslation("niego", "jego");
    translationBase.testTranslation("niego", "jego");

    translationBase.testTranslation("prezerwatywa", "eros");
    translationBase.testTranslation("prezerwatyw", "erosów");
    translationBase.testTranslation("prezerwatywami", "erosami");
    translationBase.testTranslation("prezerwatywach", "erosach");

    // To słowo może oznaczać zarówno liczbę mnogą, jak i dopełniacz liczby pojedynczej.
    // Nie powinno być przetłumaczone.
    translationBase.testTranslation("prezerwatywy", "prezerwatywy");

    translationBase.testTranslation("kondom", "eros");
    translationBase.testTranslation("kondomy", "erosy");
    translationBase.testTranslation("kondoma", "erosa");
    translationBase.testTranslation("kondomami", "erosami");
    translationBase.testTranslation("kondomach", "erosach");

    translationBase.testTranslation("więzienie", "pierdel");
    translationBase.testTranslation("uwięzienie", "uwięzienie");
    translationBase.testTranslation("więzienia", "pierdla");
    translationBase.testTranslation("uwięzienia", "uwięzienia");
    translationBase.testTranslation("więzieniach", "pierdlach");
    translationBase.testTranslation("uwięzieniach", "uwięzieniach");
    translationBase.testTranslation("więzieniu", "pierdlu");
    translationBase.testTranslation("uwięzieniu", "uwięzieniu");
    translationBase.testTranslation("więzieniem", "pierdlem");
    translationBase.testTranslation("uwięzieniem", "uwięzieniem");

    translationBase.testTranslation("choroszcz", "hjuston");
    translationBase.testTranslation("choroszczą", "hjuston");
    translationBase.testTranslation("choroszczy", "hjuston");
});