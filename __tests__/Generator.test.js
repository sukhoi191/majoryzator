'use strict';

import { Generator } from '../__modules__/Generator';
import { Person } from '../__modules__/Person';
import { SplittedTextPart } from '../__modules__/SplittedTextPart';

class SamplePerson extends Person {
    getWords() {
        return ["word"];
    }

    getFullSentences() {
        return ["Sentence."]
    }
}

const TEST_STRING = "abcdefABCDEFąćęłńóśźżĄĆĘŁŃÓŚŹŻ";
const KNOWN_ABBREVIATIONS_JSON = "{ \"abbreviations\": [ \"a.\" ] }";
const TENTATIVE_ENDS_JSON = "{ \"ends\": [ \"...\" ] }";

test('Should split text', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText(null, [' ', '\n'])).toStrictEqual([]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('test', [' ', '\n'])).toStrictEqual([new SplittedTextPart('test', null)]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('test\ntest', [' ', '\n'])).toStrictEqual([
        new SplittedTextPart('test', '\n'),
        new SplittedTextPart('test', null)
    ]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('te st', [' '])).toStrictEqual([
        new SplittedTextPart('te', ' '),
        new SplittedTextPart('st', null)
    ]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('te st\ntest', [' ', '\n'])).toStrictEqual([
        new SplittedTextPart('te', ' '),
        new SplittedTextPart('st', '\n'),
        new SplittedTextPart('test', null)
    ]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('te st', [';'])).toStrictEqual([new SplittedTextPart('te st', null)]);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).splitText('', [''])).toStrictEqual([new SplittedTextPart('', null)]);
});

test('Should check correctly for whitespaces', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly(' ')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('    ')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('\t')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('\t\n')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('\t\n  ')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly(' \t \n  \n')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly(null)).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('    a ')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isWhitespaceOnly('    \na\t')).toBe(false);
});

test('Should add welcome at the beginning', () => {
    let textArray = [
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' ')
    ];

    new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addWelcome(textArray, ['welcome']);
    expect(textArray).toStrictEqual([
        new SplittedTextPart('welcome', ' '),
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' ')
    ]);
});

test('Should add farewell at the end', () => {
    let textArray = [
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' ')
    ];

    new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addFarewell(textArray, ['farewell']);
    expect(textArray).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' '),
        new SplittedTextPart('farewell', ' ')
    ]);
});

test('Should add new sentence', () => {
    function addSentence(position, queue) {
        let textArray = [
            new SplittedTextPart('test1', ' '),
            new SplittedTextPart('test2', ' ')
        ];
        new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addNewSentence(textArray, position, queue);
        return textArray;
    }

    let queue = ['sentence'];
    expect(addSentence(0, queue)).toStrictEqual([
        new SplittedTextPart('sentence', ' '),
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(queue).toStrictEqual([]);

    queue = ['sentence'];

    expect(addSentence(1, queue)).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('sentence', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(queue).toStrictEqual([]);
    queue = ['sentence'];

    expect(addSentence(2, queue)).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' '),
        new SplittedTextPart('sentence', ' ')
    ]);

    expect(queue).toStrictEqual([]);
});

test('Should add new word with comma', () => {
    function addWord(position) {
        let textArray = [
            new SplittedTextPart('test1', ' '),
            new SplittedTextPart('test2', ' ')
        ];

        new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addNewWord(textArray, position, ['word']);
        return textArray;
    }

    expect(addWord(0)).toStrictEqual([
        new SplittedTextPart('word,', ' '),
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(addWord(1)).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('word,', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(addWord(2)).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' '),
        new SplittedTextPart('word,', ' ')
    ]);
});

test('Should be possible to add conjunction before', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore(TEST_STRING)).toBe(true);
});

test('Should not be possible to add conjunction before', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore('[' + TEST_STRING)).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore('(' + TEST_STRING)).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore('{' + TEST_STRING)).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore('-' + TEST_STRING)).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionBefore('–' + TEST_STRING)).toBe(false);
});

test('Should be possible to add conjunction after', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING)).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter('[' + TEST_STRING)).toBe(true);
});

test('Should not be possible to add conjunction after', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + ';')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + ':')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + '-')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + '–')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + '.')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + '!')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter(TEST_STRING + '?')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter('')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).canAddConjunctionAfter('a...')).toBe(false);
});

test('Should add comma', () => {
    function addComma(position) {
        let textArray = [
            new SplittedTextPart('test1', ' '),
            new SplittedTextPart('test2', ' ')
        ];
        new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addCommaBeforePositionIfNecessary(textArray, position);
        return textArray;
    }

    expect(addComma(1)).toStrictEqual([
        new SplittedTextPart('test1,', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(addComma(2)).toStrictEqual([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2,', ' ')
    ]);
});

test('Should not add comma', () => {
    function addComma(position) {
        let textArray = [
            new SplittedTextPart('test1,', ' '),
            new SplittedTextPart('test2', ' ')
        ];

        new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).addCommaBeforePositionIfNecessary(textArray, position);
        return textArray;
    }

    expect(addComma(0)).toStrictEqual([
        new SplittedTextPart('test1,', ' '),
        new SplittedTextPart('test2', ' ')
    ]);

    expect(addComma(1)).toStrictEqual([
        new SplittedTextPart('test1,', ' '),
        new SplittedTextPart('test2', ' ')
    ]);
});

test('Should join non splittable parts of text', () => {
    function join(character, splitter) {
        let textArray = [
            new SplittedTextPart(character, splitter),
            new SplittedTextPart('test1', splitter),
            new SplittedTextPart(character, splitter),
            new SplittedTextPart(character, splitter),
            new SplittedTextPart('test2', splitter),
            new SplittedTextPart(character, splitter),
            new SplittedTextPart('test3', splitter),
            new SplittedTextPart(character, splitter),
            new SplittedTextPart(character, splitter)
        ];

        new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinNonSplittableParts(textArray);
        return textArray;
    }

    function expectedResult(character, splitter) {
        return [
            new SplittedTextPart(character + splitter + 'test1', splitter),
            new SplittedTextPart(character + splitter + character + splitter + 'test2', splitter),
            new SplittedTextPart(character + splitter + 'test3', splitter),
            new SplittedTextPart(character + splitter + character, splitter)
        ];
    }

    expect(join('-', ' ')).toStrictEqual(expectedResult('-', ' '));
    expect(join('–', 'r')).toStrictEqual(expectedResult('–', 'r'));
});

test('Should join array', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinStringArray(null)).toBe('');

    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinStringArray([
        new SplittedTextPart('test1', ''),
        new SplittedTextPart('test2', ''),
        new SplittedTextPart('test3', '')
    ])).toBe('test1test2test3');

    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinStringArray([
        new SplittedTextPart('test1', ' '),
        new SplittedTextPart('test2', ' '),
        new SplittedTextPart('test3', ' ')
    ])).toBe('test1 test2 test3');

    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinStringArray([
        new SplittedTextPart('test1', ';'),
        new SplittedTextPart('test2', ';'),
        new SplittedTextPart('test3', ';')
    ])).toBe('test1;test2;test3');

    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).joinStringArray([
        new SplittedTextPart('test1', ';  '),
        new SplittedTextPart('test2', ' : '),
        new SplittedTextPart('test3', '.')
    ])).toBe('test1;  test2 : test3');
});

test('Should detect end of sentence', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('.')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '.')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('!!!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '!!!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('???')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '???')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('???!!!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '???!!!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '\\]?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '\\)?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '\\}?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('\\[' + TEST_STRING + '\\]?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('\\(' + TEST_STRING + '\\)?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('\\{' + TEST_STRING + '\\}?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '".')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '"!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '\'.')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '\'!')).toBe(true);
});

test('Should not detect end of sentence', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence('...')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + '...')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(',')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING + ',')).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndOfSentence(TEST_STRING)).toBe(false);
});


test('Should detect punctuation marks as end of sentence', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('.')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('?')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('???')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('!!!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('?!')).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isPunctuationMarksOnlyEndOfSentence('???!!!')).toBe(true);
});

test('Should be found on blacklist', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist(' a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('\'a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('"a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('[a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('(a', ['a'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('{a', ['a'])).toBe(true);
});

test('Should not be found on blacklist', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('ba', ['a'])).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('ab', ['a'])).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('a)', ['a'])).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isConjunctionOnBlacklist('a"', ['a'])).toBe(false);
});

test('Should not detect tentative end', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndingWithTentativeText('a', ['...'])).toBe(false);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndingWithTentativeText('a...a', ['...'])).toBe(false);
});

test('Should detect tentative end', () => {
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndingWithTentativeText('a...', ['...'])).toBe(true);
    expect(new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON).isEndingWithTentativeText('a...abc', ['...', 'abc'])).toBe(true);
});