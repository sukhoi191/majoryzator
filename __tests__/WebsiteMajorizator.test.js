'use strict';

import { WebsiteMajorizator } from '../__modules__/WebsiteMajorizator';
import { Generator } from '../__modules__/Generator';
import { Person } from '../__modules__/Person';

class SamplePerson extends Person {
    getWords() {
        return ["word"];
    }

    getFullSentences() {
        return ["Sentence."]
    }
}

const TEST_STRING = "abcdefABCDEFąćęłńóśźżĄĆĘŁŃÓŚŹŻ";
const CONJUNCTION_AFTER_BLACKLIST_JSON = "{ \"blacklist\": [ \"a\" ] }";
const CONJUNCTION_BEFORE_BLACKLIST_JSON = "{ \"blacklist\": [ \"a\" ] }";
const KNOWN_ABBREVIATIONS_JSON = "{ \"abbreviations\": [ \"a.\" ] }";
const TENTATIVE_ENDS_JSON = "{ \"ends\": [ \"...\" ] }";

const SAMPLE_HTML =
    '<!DOCTYPE html>' +
    '<html>' +
    '<meta charset="UTF-8">' +
    '<link rel="stylesheet" href="styles.css">' +
    '<body>' +
    '<div class="correctForMajorization">Major, to jest major. To jest dopiero Major!</div>' +
    '<div><p class="correctForMajorization">A kto na psa naszczekał?!</p></div>' +
    '<h1><div class="correctForMajorization">Siema Major!</div></h1>' +
    '<nav>' +
    '    <a href="#" class="correctForMajorization">Konon Krzysztofowicz</a>' +
    '    <a href="#" class="correctForMajorization">00jor</a>' +
    '</nav>' +
    '</body>' +
    '</html>';

function createNode(dataValue, nodeTypeValue, childNodesArray) {
    return {
        data: dataValue,
        nodeType: nodeTypeValue,
        childNodes: childNodesArray
    };
}

test('Should not be editable node', () => {
    let majorizer = new WebsiteMajorizator();

    let nonEditableNodeDataUndefined = createNode(TEST_STRING, 3, []);
    delete nonEditableNodeDataUndefined.data;

    let nonEditableNodeTypeUndefined = createNode(TEST_STRING, 3, []);
    delete nonEditableNodeTypeUndefined.nodeType;

    let nonEditableNodeChildrenUndefined = createNode(TEST_STRING, 3, []);
    delete nonEditableNodeChildrenUndefined.childNodes;

    let nonEditableNodeDataNull = createNode(null, 3, []);
    let nonEditableNodeDataEmpty = createNode("", 3, []);
    let nonEditableNodeDataNumber = createNode(0, 3, []);

    let nonEditableNodeTypeNull = createNode(TEST_STRING, null, []);
    let nonEditableNodeTypeWrong = createNode(TEST_STRING, 2, []);
    let nonEditableNodeTypeNotNumeric = createNode(TEST_STRING, TEST_STRING, []);

    expect(majorizer.isNodeEditable("")).toBe(false);
    expect(majorizer.isNodeEditable(1)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeDataUndefined)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeTypeUndefined)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeChildrenUndefined)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeDataNull)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeDataEmpty)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeDataNumber)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeTypeNull)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeTypeWrong)).toBe(false);
    expect(majorizer.isNodeEditable(nonEditableNodeTypeNotNumeric)).toBe(false);
});

test('Should be editable node', () => {
    let majorizer = new WebsiteMajorizator();

    let editableNode1 = createNode(TEST_STRING, 3, []);

    expect(majorizer.isNodeEditable(editableNode1)).toBe(true);
});

test('Should not have editable children', () => {
    let majorizer = new WebsiteMajorizator();

    let nodeNull = null;
    let nodeScript = {
        tagName: "SCRIPT"
    };
    let nodeStyle = {
        tagName: "STYLE"
    };
    let nodeDivMissingType = {
        tagName: "DIV"
    }
    let nodeDivWrongType = {
        tagName: "DIV",
        nodeType: 2
    }

    expect(majorizer.areNodeChildrenMajorizable("")).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(TEST_STRING)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(0)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(nodeNull)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(nodeScript)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(nodeStyle)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(nodeDivMissingType)).toBe(false);
    expect(majorizer.areNodeChildrenMajorizable(nodeDivWrongType)).toBe(false);
});

test('Should find correct nodes', () => {
    document.body.innerHTML = SAMPLE_HTML;
    let majorizer = new WebsiteMajorizator();
    majorizer.findNodesToMajorize(document.body);

    let expectedNodes = document.getElementsByClassName("correctForMajorization");

    expect(majorizer.nodesToMajorize.length).toBe(expectedNodes.length);

    [].forEach.call(expectedNodes, function (expectedNode) {
        expect(majorizer.nodesToMajorize).toContain(expectedNode.childNodes[0]);
    });
});

test('Should majorize nodes', () => {
    let sampleNodesToMajorize = [{
        data: "A"
    },
    {
        data: "B"
    },
    {
        data: "C"
    }
    ];

    let generator = new Generator(new SamplePerson(), KNOWN_ABBREVIATIONS_JSON, TENTATIVE_ENDS_JSON);
    const generateMock = generator.generate = jest.fn();

    let majorizer = new WebsiteMajorizator();
    majorizer.nodesToMajorize = JSON.parse(JSON.stringify(sampleNodesToMajorize));

    majorizer.majorizeNodes(generator, CONJUNCTION_BEFORE_BLACKLIST_JSON, CONJUNCTION_AFTER_BLACKLIST_JSON);

    expect(generateMock).toHaveBeenCalledTimes(majorizer.nodesToMajorize.length);
    expect(generateMock.mock.calls[0][1]).toBe(true);
    expect(generateMock.mock.calls[0][2]).toBe(false);

    for (let i = 1; i < generateMock.mock.calls.length - 1; i++) {
        expect(generateMock.mock.calls[i][1]).toBe(false);
        expect(generateMock.mock.calls[i][2]).toBe(false);
    }

    expect(generateMock.mock.calls[generateMock.mock.calls.length - 1][1]).toBe(false);
    expect(generateMock.mock.calls[generateMock.mock.calls.length - 1][2]).toBe(true);

    for (let i = 0; i < generateMock.mock.calls.length; i++) {
        expect(generateMock.mock.calls[i][0]).toBe(sampleNodesToMajorize[i].data);
    }
});