'use strict';

import { Translator } from '../__modules__/Translator';
import { SplittedTextPart } from '../__modules__/SplittedTextPart';

const SAMPLE_TRANSLATIONS_JSON = {
    "translations": [{
        "sourceWord": "abc",
        "operations": [{
            "method": "replaceWithCasing",
            "stringToReplace": "abc",
            "replacementString": "cba"
        }],
    },
    {
        "sourceWord": "wholeword",
        "operations": [{
            "method": "replaceWithCasing",
            "stringToReplace": "whole",
            "replacementString": "all",
            "matchWholeWord": true
        }]
    },
    {
        "sourceWord": "beginonly",
        "operations": [{
            "method": "replaceWithCasing",
            "stringToReplace": "begin",
            "replacementString": "worked",
            "type": "begin_only"
        }]
    },
    {
        "sourceWord": "insideonly",
        "operations": [{
            "method": "replaceWithCasing",
            "stringToReplace": "inside",
            "replacementString": "worked",
            "type": "inside_only"
        }]
    },
    {
        "sourceWord": "endonly",
        "operations": [{
            "method": "replaceWithCasing",
            "stringToReplace": "end",
            "replacementString": "worked",
            "type": "end_only"
        }]
    }
    ]
};

test('Should change single letters case-sensitive', () => {
    expect(new Translator().replaceWithCasing('test', 'test', ['t', 'c'])).toBe('cesc');
    expect(new Translator().replaceWithCasing('TesT', 'TesT', ['t', 'c'])).toBe('CesC');
    expect(new Translator().replaceWithCasing('TeST', 'TeST', ['t', 'c'])).toBe('CeSC');
    expect(new Translator().replaceWithCasing('TEsT', 'TEsT', ['t', 'c'])).toBe('CEsC');
    expect(new Translator().replaceWithCasing('tEsT', 'tEsT', ['t', 'c'])).toBe('cEsC');
});

test('Should remove character', () => {
    expect(new Translator().replaceWithCasing('test', 'test', ['t', ''])).toBe('es');
});

test('Should leave whitespaces untouched', () => {
    expect(new Translator().replaceWithCasing('t  es   t', 't  es   t', ['t', 'c'])).toBe('c  es   c');
    expect(new Translator().replaceWithCasing('t\tes\nt', 't\tes\nt', ['t', 'c'])).toBe('c\tes\nc');
});

test('Should leave whitespaces untouched', () => {
    expect(new Translator().replaceWithCasing('t  es   t', 't  es   t', ['t', 'c'])).toBe('c  es   c');
    expect(new Translator().replaceWithCasing('t\tes\nt', 't\tes\nt', ['t', 'c'])).toBe('c\tes\nc');
});

test('Should translate simple text', () => {
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('abc', ' ')])).toStrictEqual([new SplittedTextPart('cba', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('ABC', ' ')])).toStrictEqual([new SplittedTextPart('CBA', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('Abc', ' ')])).toStrictEqual([new SplittedTextPart('Cba', ' ')]);
});

test('Should translate whole word only', () => {
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('wholeword', ' ')])).toStrictEqual([new SplittedTextPart('allword', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('wholewordonly', ' ')])).toStrictEqual([new SplittedTextPart('wholewordonly', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('onlywholeword', ' ')])).toStrictEqual([new SplittedTextPart('onlywholeword', ' ')]);
});

test('Should translate only when string is in the beginning of the text', () => {
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('beginonly', ' ')])).toStrictEqual([new SplittedTextPart('workedonly', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('beginonlytest', ' ')])).toStrictEqual([new SplittedTextPart('workedonlytest', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('notbegintest', ' ')])).toStrictEqual([new SplittedTextPart('notbegintest', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('notbegin', ' ')])).toStrictEqual([new SplittedTextPart('notbegin', ' ')]);
});

test('Should translate only when string is in the middle of the text', () => {
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('endonly', ' ')])).toStrictEqual([new SplittedTextPart('workedonly', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('endonlytest', ' ')])).toStrictEqual([new SplittedTextPart('endonlytest', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('isendonlytest', ' ')])).toStrictEqual([new SplittedTextPart('isendonlytest', ' ')]);
    expect(new Translator(SAMPLE_TRANSLATIONS_JSON.translations).translate([new SplittedTextPart('testendonly', ' ')])).toStrictEqual([new SplittedTextPart('testworkedonly', ' ')]);
});

test('Should capitalize first letter', () => {
    expect(new Translator().capitalizeFirstLetter('abc')).toBe('Abc');
    expect(new Translator().capitalizeFirstLetter('ąbc')).toBe('Ąbc');
    expect(new Translator().capitalizeFirstLetter('ąBC')).toBe('ĄBC');
});