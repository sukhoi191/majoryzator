/**
 * Tłumaczymy?
 */
export const TRANSLATE = true;

/**
 * Wstawiamy pełne zdania?
 */
export const ADD_SENTENCES = true;

/**
 * Wstawiamy spójniki?
 */
export const ADD_WORDS = true;

/**
 * Wstawiamy powitanie?
 */
export const ADD_WELCOME = true;

/**
 * Wstawiamy pożegnanie?
 */
export const ADD_FAREWELL = true;

/**
 * Fragment wyrażenia regularnego dla wyszukiwania liter.
 */
export const REGEX_LETTERS = "a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ";