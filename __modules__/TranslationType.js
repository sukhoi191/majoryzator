/**
 * Enumeracja dla typu tłumaczenia.
 */
export const TranslationType = {
    ANY: 1,
    BEGIN_ONLY: 2,
    INSIDE_ONLY: 3,
    END_ONLY: 4
};