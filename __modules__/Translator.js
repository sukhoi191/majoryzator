import { TranslationType } from './TranslationType';
import { TranslationCapitalizationType} from './TranslationCapitalizationType';
/**
 * Translator.
 */
export class Translator {
    constructor(translations) {
        this.translations = translations;
    }

    /**
     * Podmienia podane stringi w tekście, dodatkowo podmieniając wersje lowercase
     * i uppercase podanych stringów.
     * @param {string} str String do zmiany.
     * @param {string} originalWord Oryginalne słowo do zamiany.
     * @param {string} findReplace Dane zmiany.
     * @param {TranslationType} translationType Określa typ tłumaczenia.
     * @param {boolean} matchWholeWord Czy wykonać tłumaczenie tylko w przypadku znalezienia całego słowa?
     */
    replaceWithCasing(str, originalWord, findReplace, translationType = TranslationType.ANY,
        matchWholeWord = false) {
        if (matchWholeWord && str.toLowerCase() != originalWord.toLowerCase()) {
            return str;
        }

        let indexSearchResult = this.findIndex(str, originalWord);
        let index = indexSearchResult[0];

        if (index == -1) {
            return str;
        }

        if (translationType == TranslationType.BEGIN_ONLY && index > 0) {
            return str;
        }

        if (translationType == TranslationType.INSIDE_ONLY && (index == 0 || index + originalWord.length == str.length)) {
            return str;
        }

        if (translationType == TranslationType.END_ONLY && index + originalWord.length != str.length) {
            return str;
        }

        let swapped = originalWord;
        let find = findReplace[0];
        let replace = findReplace[1];

        swapped = this.replaceExact(swapped, find, replace);
        swapped = this.replaceExact(swapped, find.toLowerCase(), replace.toLowerCase());
        swapped = this.replaceExact(swapped, find.toUpperCase(), replace.toUpperCase());

        if (indexSearchResult[1] == TranslationCapitalizationType.LOWERCASE) {
            swapped = swapped.toLowerCase();
        } else if (indexSearchResult[1] == TranslationCapitalizationType.UPPERCASE) {
            swapped = swapped.toUpperCase();
        } else if (indexSearchResult[1] == TranslationCapitalizationType.TITLECASE) {
            swapped = this.capitalizeFirstLetter(swapped);
        }

        return str.substring(0, index) + swapped + str.substring(index + originalWord.length);
    }

    /**
     * Wyszukuje indeks wystąpienia podanego tekstu w innym tekście.
     * Sprawdza różne możliwości wielkości liter.
     * @param {string} str Tekst do przeszukania.
     * @param {string} originalWord Tekst do znalezienia.
     * @returns Lista elementów - pierwszy to indeks, drugi określa sposób,
     * w jaki znaleziono tekst: N = bez zmian, L = lowercase, U = uppercase,
     * ? = nie znaleziono.
     */
    findIndex(str, originalWord) {
        let index = str.indexOf(originalWord);

        if (index != -1) {
            return [index, TranslationCapitalizationType.NOT_FOUND];
        }

        index = str.indexOf(originalWord.toLowerCase());

        if (index != -1) {
            return [index, TranslationCapitalizationType.LOWERCASE];
        }

        index = str.indexOf(originalWord.toUpperCase());

        if (index != -1) {
            return [index, TranslationCapitalizationType.UPPERCASE];
        }

        index = str.indexOf(this.capitalizeFirstLetter(originalWord));

        if (index != -1) {
            return [index, TranslationCapitalizationType.TITLECASE];
        }

        return [index, TranslationCapitalizationType.UNKNOWN];
    }

    /**
     * Zmienia pierwszą literę tekstu na wielką.
     * @param {string} str Tekst do zmiany.
     */
    capitalizeFirstLetter(str) {
        return str[0].toLocaleUpperCase() + str.substring(1);
    }

    /**
     * Dokonuje dokładną zamianę, bez rozróżnienia na wielkość liter.
     * @param {string} str String do zmiany.
     * @param {string} find String do znalezienia.
     * @param {string} replaceWord Tekst do wstawienia.
     */
    replaceExact(str, find, replaceWord) {
        return str.replace(new RegExp(find, "g"), replaceWord);
    }

    /**
     * Wykonuje tłumaczenie.
     * @param {array} textArray Tablica tekstu do przetłumaczenia.
     */
    translate(textArray) {
        textArray.forEach(function (text) {
            this.translations.forEach(function (translation) {
                let sourceWord = translation["sourceWord"];

                if (!new RegExp(translation["sourceWord"], "i").test(text.getText())) {
                    return;
                }

                let translatedWord = sourceWord;

                translation["operations"].forEach(function (operation) {
                    if (operation["method"] == "replaceWithCasing") {
                        let stringToReplace = operation["stringToReplace"];
                        let replacementString = operation["replacementString"];
                        let type = TranslationType.ANY;
                        let matchWholeWord = false;

                        if ("type" in operation) {
                            switch (operation["type"]) {
                                case "any":
                                    type = TranslationType.ANY;
                                    break;
                                case "begin_only":
                                    type = TranslationType.BEGIN_ONLY;
                                    break;
                                case "inside_only":
                                    type = TranslationType.INSIDE_ONLY;
                                    break;
                                case "end_only":
                                    type = TranslationType.END_ONLY;
                                    break;
                            }
                        }

                        if ("matchWholeWord" in operation) {
                            matchWholeWord = operation["matchWholeWord"];
                        }

                        text.setText(this.replaceWithCasing(text.getText(), translatedWord, [stringToReplace, replacementString], type, matchWholeWord));
                        translatedWord = text.getText();
                    }
                }.bind(this));
            }.bind(this));
        }.bind(this));

        return textArray;
    }
}