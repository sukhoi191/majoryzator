import { Person } from './Person';

export class PersonKonon extends Person {
    constructor() {
        super('Krzysztof Kononowicz');
    }

    getWords() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("kononWordsJson")).words);
    }

    getFullSentences() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("kononSentencesJson")).sentences);
    }

    getWelcomes() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("kononWelcomesJson")).welcomes);
    }

    getFarewells() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("kononFarewellsJson")).farewells);
    }

    getTranslations() {
        return JSON.parse(GM_getResourceText("kononTranslationsJson")).translations;
    }

    getProbabilityOfAddingNewWord() {
        return 1;
    }

    getProbabilityOfAddingNewSentence() {
        return 40;
    }

    getCorrectWelcomeForTime(date) {
        let welcome = super.getCorrectWelcomeForTime(date);
        welcome = welcome[0].toUpperCase() + welcome.substr(1);
        return welcome + " państwu";
    }
}