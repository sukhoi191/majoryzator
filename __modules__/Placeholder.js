export class Placeholder {
    constructor(content, func) {
        this.content = content;
        this.func = func;
    }

    swap(textArray) {
        for (let i = 0; i < textArray.length; i++) {
            textArray[i] = textArray[i].replace(this.content, this.func());
        }

        return textArray;
    }
}