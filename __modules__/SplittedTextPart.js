export class SplittedTextPart {
    constructor(text, splitter) {
        this.text = text;
        this.splitter = splitter;
    }

    getText() {
        return this.text;
    }

    setText(text) {
        this.text = text;
    }

    getSplitter() {
        return this.splitter;
    }

    setSplitter(splitter) {
        this.splitter = splitter;
    }

    removeSplitter() {
        this.setSplitter(null);
    }

    getJoined() {
        return this.getText() + (this.getSplitter() != null ? this.getSplitter() : "");
    }

    toString() {
        return 'text: "' + this.getText() + '" | splitter: "' + this.getSplitter() + '"';
    }
}