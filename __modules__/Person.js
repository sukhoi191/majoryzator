import { Placeholder } from "./Placeholder";

/**
 * Reprezentuje osobę.
 */
export class Person {
    constructor(name) {
        this.name = name;

        this.welcomeSwaps = [
            new Placeholder("{PrzywitaniePoraDnia}", () => this.getCorrectWelcomeForTime(new Date())),
            new Placeholder("{DzienTygodnia}", () => this.getDayOfWeek(new Date())),
            new Placeholder("{Data}", () => this.getDateAsString(new Date())),
            new Placeholder("{Godzina}", () => this.getTimeAsString(new Date()))
        ];
    }

    /**
     * Imię i nazwisko osoby.
     */
    getName() {
        return this.name;
    }

    /**
     * Słowa ("spójniki") do wstawienia.
     */
    getWords() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Pełne zdania do wstawienia.
     */
    getFullSentences() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Powitania.
     */
    getWelcomes() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Pożegnania.
     */
    getFarewells() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Prawdopodobieństwo, że po dotarciu do miejsca, w którym można dodać nowe słowo,
     * zostanie ono wstawione.
     * 0 oznacza 0% szansy, 100 oznacza 100% szansy.
     */
    getProbabilityOfAddingNewWord() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Prawdopodobieństwo, że po dotarciu do miejsca, w którym można dodać nowe zdanie,
     * zostanie ono wstawione.
     * 0 oznacza 0% szansy, 100 oznacza 100% szansy.
     */
    getProbabilityOfAddingNewSentence() {
        throw new Error('Method is not implemented.');
    }

    /**
     * Lista tłumaczeń.
     * Każdy element listy składa się z kilku informacji:
     *
     * [słowo do znalezienia, funkcja zamieniająca słowo]
     *
     * Funkcja zamieniająca słowo przyjmuje jako parametr tekst do przetłumaczenia
     * oraz instancję translatora.
     *
     * Standardową implementacją funkcji jest zamiana poszczególnych liter w słowie.
     * Dla przykładu, jeśli element poniższej listy będzie wyglądał tak:
     *
     * ["Cześć", function(str, instane) {
     *   return instance.replaceWithCasing(str, [["ś", "si"], ["ć", "ci"]])
     * }]
     *
     * W tej sytuacji każde wystąpienie litery "ś" zostanie zmienione na "si", a każde
     * wystąpienie litery "ć" zostanie zmienione na "ci", co da nam jako wynik "czesici"
     * (tak, to cudny przykład).
     *
     * Użycie metody replaceWithCasing() pozwala na uporanie się z możliwością otrzymania
     * stringa z różnymi wielkościami liter. Dzięki temu przetłumaczone zostaną wszelkie
     * możliwe opcje:
     * - Cześć
     * - cześć
     * - cZeŚĆ
     * - CZEŚĆ
     * - itd.
     */
    getTranslations() {
        throw new Error('Method is not implemented.');
    }

    getCorrectWelcomeForTime(date) {
        if (date.getHours() >= 19 || (date.getHours() >= 0 && date.getHours() <= 4)) {
            return "dobry wieczór";
        } else {
            return "dzień dobry";
        }
    }

    swapPlaceholders(textArray) {
        for (let i = 0; i < this.welcomeSwaps.length; i++) {
            textArray = this.welcomeSwaps[i].swap(textArray);
        }

        return textArray;
    }

    getTimeAsString(date) {
        // https://stackoverflow.com/a/12230363/3470545
        return ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);
    }

    getDateAsString(date) {
        let month = '';
        switch (date.getMonth()) {
            case 0:
                month = "stycznia";
                break;
            case 1:
                month = "lutego";
                break;
            case 2:
                month = "marca";
                break;
            case 3:
                month = "kwietnia";
                break;
            case 4:
                month = "maja";
                break;
            case 5:
                month = "czerwca";
                break;
            case 6:
                month = "lipca";
                break;
            case 7:
                month = "sierpnia";
                break;
            case 8:
                month = "września";
                break;
            case 9:
                month = "października";
                break;
            case 10:
                month = "listopada";
                break;
            case 11:
                month = "grudnia";
                break;
        }
        return date.getDate() + " " + month;
    }

    getDayOfWeek(date) {
        switch (date.getDay()) {
            case 0:
                return "niedzielę";
            case 1:
                return "poniedziałek";
            case 2:
                return "wtorek";
            case 3:
                return "środę";
            case 4:
                return "czwartek";
            case 5:
                return "piątek";
            case 6:
                return "sobotę";
            default:
                return "chuj wie co";
        }
    }
}