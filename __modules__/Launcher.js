import { WebsiteMajorizator } from './WebsiteMajorizator';
import { PersonMajor } from './PersonMajor';
import { PersonKonon } from './PersonKonon';

export class Launcher {
    addCss() {
        let style = document.createElement("style");
        style.innerHTML = GM_getResourceText("css");
        document.body.appendChild(style);
    }

    createButton(person, text, buttonId) {
        let button = document.createElement("input");
        button.type = "button";
        button.value = text;
        button.classList.add("majoryzator-button");
        button.id = buttonId;
        button.onclick = () => new WebsiteMajorizator(person).majorizeWholeWebsite(
            GM_getResourceText("conjunctionBeforeBlacklistJson"),
            GM_getResourceText("conjunctionAfterBlacklistJson"),
            GM_getResourceText("knownAbbreviationsJson"),
            GM_getResourceText("tentativeEndsJson")
        );
        document.body.appendChild(button);
    }

    launch() {
        this.addCss();
        this.createButton(new PersonMajor(), "M", "majoryzator-suchodolski");
        this.createButton(new PersonKonon(), "K", "majoryzator-kononowicz");
    }
}

new Launcher().launch();