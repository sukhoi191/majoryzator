import { ADD_SENTENCES, ADD_WORDS, ADD_WELCOME, ADD_FAREWELL, REGEX_LETTERS, TRANSLATE } from './Configuration.Functional'
import { Translator } from './Translator';
import { SplittedTextPart } from './SplittedTextPart';
import { Person } from './Person';

let _ = require('lodash');

/**
 * Generator majorowych wstawek w tekście.
 */
export class Generator {

    /**
     * Tworzy nową instancję.
     * @param {Person} person Osoba.
     * @param {string} knownAbbreviationsJson Znane skróty.
     * @param {string} tentativeEndsJson Niepewne końcówki.
     */
    constructor(person, knownAbbreviationsJson, tentativeEndsJson) {
        this.person = person;
        this.wordsArray = person.getWords();
        this.sentencesArray = person.getFullSentences();
        this.textSplitters = [' ', '\n'];
        this.knownAbbreviations = JSON.parse(knownAbbreviationsJson).abbreviations;
        this.tentativeEnds = JSON.parse(tentativeEndsJson).ends;

        this.createWordsQueue();
        this.createSentencesQueue();
    }

    /**
     * Tworzy kolejkę spójników.
     */
    createWordsQueue() {
        this.wordsQueue = this.wordsArray.slice(0);
        this.shuffleArray(this.wordsQueue);
    }

    /**
     * Tworzy kolejkę zdań.
     */
    createSentencesQueue() {
        this.sentencesQueue = this.sentencesArray.slice(0);
        this.shuffleArray(this.sentencesQueue);
    }

    /**
     * Miesza elementy w tablicy (generuje losową permutację).
     * Kod z https://stackoverflow.com/a/12646864/3470545 (a co, ja też jestem
     * czasem leniwy).
     * @param {array} array Tablica do wymieszania.
     */
    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    /**
     * Losowo wstawia słowa / zdania do tekstu.
     * @param {string} text Tekst do zmiany.
     * @param {boolean} addWelcomeNow Czy wstawiamy powitanie na początku podanego tekstu?
     * @param {boolean} addFarewellNow Czy wstawiamy pożegnanie na końcu podanego tekstu?
     * @param {string} conjunctionBeforeBlacklist Czarna lista słów, przed którymi nie można wstawić spójnika.
     * @param {string} conjunctionAfterBlacklistJson Czarna lista słów, po których nie można wstawić spójnika.
     */
    generate(text, addWelcomeNow, addFarewellNow, conjunctionBeforeBlacklistJson, conjunctionAfterBlacklistJson) {
        let splittedText = this.splitText(text, this.textSplitters);

        let conjunctionBeforeBlacklist = JSON.parse(conjunctionBeforeBlacklistJson).blacklist;
        let conjunctionAfterBlacklist = JSON.parse(conjunctionAfterBlacklistJson).blacklist;

        this.joinNonSplittableParts(splittedText);
        this.majorizeTextArray(splittedText, conjunctionBeforeBlacklist, conjunctionAfterBlacklist);

        if (addWelcomeNow && ADD_WELCOME) {
            this.addWelcome(splittedText, this.person.getWelcomes());
        }

        if (addFarewellNow && ADD_FAREWELL) {
            this.addFarewell(splittedText, this.person.getFarewells());
        }

        if (TRANSLATE) {
            this.translate(splittedText);
        }

        return this.joinStringArray(splittedText);
    }

    /**
     * Tłumaczy podaną tablicę tekstu.
     * @param {array} textArray Tablica ze słowami do przetłumaczenia.
     */
    translate(textArray) {
        textArray = new Translator(this.person.getTranslations()).translate(textArray);
    }

    /**
     * Dzieli tekst w miejcach wystąpienia podanych znaków.
     * @param {string} text Tekst do podzielenia.
     * @param {array} splittersArray Tablica z tekstami dzielącymi.
     */
    splitText(text, splittersArray) {
        if (text == null) {
            return [];
        }

        let result = [new SplittedTextPart(text, '')];

        splittersArray.forEach(function (splitter) {
            result = this.splitTextWith(result, splitter);
        }.bind(this));

        return result;
    }

    /**
     * Dzieli elementy na tablicy tekstowej w miejscach wystąpienia podanego tekstu.
     * @param {string} textArray Tablica z tekstem.
     * @param {string} splitter Tekst dzielący.
     */
    splitTextWith(textArray, splitter) {
        let newArray = [];

        textArray.forEach(splittedTextPart => {
            let splitted = splittedTextPart.getText().split(splitter);

            if (splitted.length <= 1) {
                newArray.push(splittedTextPart);
            } else {
                splitted.forEach(element => newArray.push(new SplittedTextPart(element, splitter)));
                newArray[newArray.length - 1].setSplitter(splittedTextPart.getSplitter());
            }
        });

        if (newArray.length > 0) {
            newArray[newArray.length - 1].removeSplitter();
        }

        return newArray;
    }

    /**
     * Dokonuje majoryzacji tekstu.
     * @param {array} textArray Tablica ze słowami do majoryzacji.
     * @param {array} conjunctionBeforeBlacklist Czarna lista słów, przed którymi nie można wstawić spójnika.
     * @param {array} conjunctionAfterBlacklist Czarna lista słów, po których nie można wstawić spójnika.
     */
    majorizeTextArray(textArray, conjunctionBeforeBlacklist, conjunctionAfterBlacklist) {
        for (let i = 1; i <= textArray.length; i++) {
            if (i < textArray.length &&
                (this.isWhitespaceOnly(textArray[i].getText()) ||
                    this.isEndingWithTentativeText(textArray[i].getText(), this.tentativeEnds))) {
                continue;
            }

            let addNewSentence = false;
            let addNewWord = false;

            addNewSentence = this.shouldAddNewSentence(this.person.getProbabilityOfAddingNewSentence()) &&
                this.isEndOfSentence(textArray[i - 1].getText());

            if (!addNewSentence) {
                addNewWord = this.shouldAddNewWord(this.person.getProbabilityOfAddingNewWord());
            }

            if (ADD_SENTENCES && addNewSentence) {
                if (this.sentencesQueue.length == 0) {
                    this.createSentencesQueue();
                }

                let addedWordsCount = this.addNewSentence(textArray, i, this.sentencesQueue);
                i += addedWordsCount;
            } else if (ADD_WORDS && addNewWord) {
                if (this.wordsQueue.length == 0) {
                    this.createWordsQueue();
                }

                if (!this.canAddWordBetweenWords(textArray, i, conjunctionAfterBlacklist, conjunctionBeforeBlacklist)) {
                    continue;
                }

                let addedWordsCount = this.addNewWord(textArray, i, this.wordsQueue);
                this.addCommaBeforePositionIfNecessary(textArray, i);
                i += addedWordsCount;
            }
        }
    }

    /**
     * Sprawdza, czy w podanym miejscu można wstawić nowe słowo, biorąc pod uwagę
     * słowa znajdujące się bezpośrednio po i przed obecnie wstawianym.
     * @param {array} textArray Tablica ze słowami do majoryzacji.
     * @param {number} position Pozycja nowego słowa.
     * @param {array} conjunctionBeforeBlacklist Czarna lista słów, przed którymi nie można wstawić spójnika.
     * @param {array} conjunctionAfterBlacklist Czarna lista słów, po których nie można wstawić spójnika.
     */
    canAddWordBetweenWords(textArray, position, conjunctionAfterBlacklist, conjunctionBeforeBlacklist) {
        if (position == 0 || position >= textArray.length) {
            return false;
        }

        return this.canAddConjunctionAfter(textArray[position - 1].getText(), conjunctionAfterBlacklist) &&
            this.canAddConjunctionBefore(textArray[position].getText(), conjunctionBeforeBlacklist);
    }

    /**
     * Sprawdza, czy podany tekst składa się tylko z białych znaków.
     * @param {string} str Tekst do sprawdzenia.
     */
    isWhitespaceOnly(str) {
        return str == "" || /^\s*$/.test(str);
    }

    /**
     * Biorąc pod uwagę prawdopodobieństwo dodania nowego słowa, generuje liczbę informującą o tym,
     * czy zostanie ono dodane.
     * @param {number} probabilityOfNewWord Prawdopodobieństwo dodania nowego słowa.
     */
    shouldAddNewWord(probabilityOfNewWord) {
        return _.random(0, 100, false) < probabilityOfNewWord;
    }

    /**
     * Biorąc pod uwagę prawdopodobieństwo dodania nowego zdania, generuje liczbę informującą o tym,
     * czy zostanie ono dodane.
     * @param {number} probabilityOfNewSentence Prawdopodobieństwo dodania nowego zdania.
     */
    shouldAddNewSentence(probabilityOfNewSentence) {
        return _.random(0, 100, false) < probabilityOfNewSentence;
    }

    /**
     * Biorąc pod uwagę prawdopodobieństwo dodania nowej czynności, generuje liczbę informującą o tym,
     * czy zostanie ona dodana.
     * @param {number} probabilityOfNewAction Prawdopodobieństwo dodania nowej czynności.
     */
    shouldAddNewAction(probabilityOfNewAction) {
        return _.random(0, 100, false) < probabilityOfNewAction;
    }

    /**
     * Dodaje losowe powitanie.
     * @param {array} textArray Tablica ze słowami.
     * @param {array} welcomes Tablica z dostępnymi powitaniami.
     */
    addWelcome(textArray, welcomes) {
        let splittedWelcome = this.splitText(_.sample(welcomes), this.textSplitters);
        splittedWelcome[splittedWelcome.length - 1].setSplitter(' ');
        Array.prototype.unshift.apply(textArray, [].concat(splittedWelcome));
    }

    /**
     * Dodaje losowe pożegnanie.
     * @param {array} textArray Tablica ze słowami.
     * @param {array} farewells Tablica z dostępnymi pożegnaniami.
     */
    addFarewell(textArray, farewells) {
        textArray[textArray.length - 1].setSplitter(' ');

        let splittedFarewell = this.splitText(_.sample(farewells), this.textSplitters);
        splittedFarewell[splittedFarewell.length - 1].setSplitter(' ');
        Array.prototype.push.apply(textArray, [].concat(splittedFarewell));
    }

    /**
     * Wstawia losowe zdanie na podanej pozycji.
     * @param {array} textArray Tablica ze słowami.
     * @param {number} position Pozycja do wstawienia nowego zdania.
     * @param {array} sentencesQueue Kolejka z dostępnymi zdaniami.
     * @returns Ilość słów w dodanej tablicy.
     */
    addNewSentence(textArray, position, sentencesQueue) {
        if (position == textArray.length) {
            textArray[position - 1].setSplitter(' ');
        }

        let splittedSentence = this.splitText(sentencesQueue.shift(), this.textSplitters);
        splittedSentence[splittedSentence.length - 1].setSplitter(' ');
        Array.prototype.splice.apply(textArray, [position, 0].concat(splittedSentence));
        return splittedSentence.length;
    }

    /**
     * Wstawia nowe losowe słowo na podanej pozycji i dopisuje przecinek.
     * @param {array} textArray Tablica ze słowami.
     * @param {number} position Pozycja do wstawienia nowego zdania.
     * @param {array} wordsQueue Kolejka z dostępnymi słowami.
     * @returns Ilość słów w dodanej tablicy.
     */
    addNewWord(textArray, position, wordsQueue) {
        let splittedWord = this.splitText(wordsQueue.shift() + ",", this.textSplitters);
        splittedWord[splittedWord.length - 1].setSplitter(' ');
        Array.prototype.splice.apply(textArray, [position, 0].concat(splittedWord));
        return splittedWord.length;
    }

    /**
     * Sprawdza, czy można wstawić spójnik przed podanym tekstem.
     * @param {string} text Tekst do sprawdzenia.
     * @param {array} conjunctionBeforeBlacklist Czarna lista słów, przed którymi nie można wstawić spójnika.
     */
    canAddConjunctionBefore(text, conjunctionBeforeBlacklist = []) {
        return !/[\[|\(|\{|\-|–].*/.test(text) &&
            !this.isConjunctionOnBlacklist(text, conjunctionBeforeBlacklist);
    }

    /**
     * Sprawdza, czy można wstawić spójnik za podanym tekstem.
     * @param {string} text Tekst do sprawdzenia.
     * @param {array} conjunctionBeforeBlacklist Czarna lista słów, po których nie można wstawić spójnika.
     */
    canAddConjunctionAfter(text, conjunctionAfterBlacklist = []) {
        return !this.isWhitespaceOnly(text) &&
            !this.isEndOfSentence(text) &&
            !/.*[;|:|\-|–|\)\]\}]$/.test(text) &&
            !this.isConjunctionOnBlacklist(text, conjunctionAfterBlacklist) &&
            !this.isEndingWithTentativeText(text, this.tentativeEnds);
    }

    /**
     * Sprawdza, czy podany tekst jest na czarnej liście słów.
     * @param {string} text Tekst do sprawdzenia.
     */
    isConjunctionOnBlacklist(text, blacklist) {
        for (let i = 0; i < blacklist.length; i++) {
            if (new RegExp('^[ "\'\\(\\[\\{]*(' + blacklist[i].toLowerCase() + ')$').test(text.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Dodaje przecinek przed podaną pozycję, o ile jest to potrzebne.
     * @param {array} textArray Tablica ze słowami.
     * @param {number} position Pozycja do sprawdzenia.
     */
    addCommaBeforePositionIfNecessary(textArray, position) {
        if (position > 0 && position <= textArray.length && !textArray[position - 1].getText().endsWith(",")) {
            textArray[position - 1].setText(textArray[position - 1].getText() + ",");
        }
    }

    addSpaceIfNecessary(textArray, position) {
        if (position > 0 && position <= textArray.length && textArray[position - 1].getSplitter() == null) {
            textArray[position - 1].setText(textArray[position - 1].getText() + " ");
        }
    }

    /**
     * Łączy te części tekstu, które nie powinny być podzielone.
     * @param {array} textArray Tablica z tekstem.
     */
    joinNonSplittableParts(textArray) {
        let nonSplitabbleParts = [
            "-", "–", ""
        ];

        for (let i = 0; i < textArray.length; i++) {
            let startIndex = -1;

            if (nonSplitabbleParts.includes(textArray[i].getText())) {
                startIndex = i;
            }

            while (i < textArray.length && nonSplitabbleParts.includes(textArray[i].getText())) {
                // Znajdujemy następny dozwolony znak.
                i++;
            }

            if (i == textArray.length) {
                i--;
            }

            if (startIndex != -1) {
                let slice = textArray.slice(startIndex, i + 1);
                let newElement = '';
                let lastSplitter = '';

                slice.forEach(function (element) {
                    newElement += element.getJoined();
                    lastSplitter = element.getSplitter();
                });

                if (lastSplitter != null) {
                    newElement = newElement.slice(0, -lastSplitter.length);
                }

                textArray.splice(startIndex, i - startIndex);
                textArray[startIndex].setText(newElement);
                textArray[startIndex].setSplitter(lastSplitter);
                i -= (i - startIndex);
            }
        }
    }

    /**
     * Łączy tablicę tekstu do stringa, używając podanego znaku lub tekstu do połączenia
     * kolejnych elementów.
     * @param {array} textArray Tablica ze słowami.
     */
    joinStringArray(textArray) {
        if (textArray == null) {
            return '';
        }

        let str = "";
        let lastSplitter = '';

        textArray.forEach(element => {
            str += element.getJoined();
            lastSplitter = element.getSplitter();
        });

        if (lastSplitter == null) {
            return str;
        }

        return str.length > 0 && lastSplitter.length > 0 ? str.slice(0, -lastSplitter.length) : str;
    }

    /**
     * Określa, czy podany string jest końcem zdania.
     * @param {string} str Tekst do sprawdzenia.
     */
    isEndOfSentence(str) {
        if (this.isAbbreviation(str, this.knownAbbreviations)) {
            return false;
        }

        let endOfSentence = new RegExp('[' + REGEX_LETTERS + '0-9 \\[\\{\\(\\]\\}\\)"\']+(\\.|[!|?]+)$').test(str);

        if (endOfSentence) {
            return true;
        }

        if (this.isPunctuationMarksOnlyEndOfSentence(str)) {
            return true;
        }

        return false;
    }

    /**
     * Sprawdza, czy podany tekst zawiera tylko znaki interpunkcyjne, które określają koniec zdania,
     * np. kropka, wykrzyknik (lub więcej) itd.
     * @param {string} str Tekst do sprawdzenia.
     */
    isPunctuationMarksOnlyEndOfSentence(str) {
        if (str.length == 0) {
            return false;
        }

        if (str.length == 1 && str[0] == '.') {
            return true;
        }

        for (let i = 0; i < str.length; i++) {
            if (str[i] != '!' && str[i] != '?') {
                return false;
            }
        }

        return true;
    }

    /**
     * Sprawdza, czy podany tekst jest znanym skrótem.
     * @param {string} str Tekst do sprawdzenia.
     * @param {array} abbreviations Tablica znanych skrótów.
     */
    isAbbreviation(str, abbreviations) {
        for (let i = 0; i < abbreviations.length; i++) {
            let abbreviation = abbreviations[i];

            if (new RegExp('^[ "\'\\(\\[\\{]*(' + abbreviation.toLowerCase() + ')$').test(str.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sprawdza, czy podany tekst kończy się znakami, które nie dają pewności
     * pod względem tego, czy mamy do czynienia z końcem zdania, czy nie.
     * @param {string} str Tekst do sprawdzenia.
     * @param {array} tentativeEndsArray Tablica niepewnych zakończeń.
     */
    isEndingWithTentativeText(str, tentativeEndsArray) {
        for (let i = 0; i < tentativeEndsArray.length; i++) {
            if (str.toLowerCase().endsWith(tentativeEndsArray[i])) {
                return true;
            }
        }

        return false;
    }
}