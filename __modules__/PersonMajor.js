import { Person } from './Person';

export class PersonMajor extends Person {
    constructor() {
        super('Major Wojciech Suchodoski');
    }

    getWords() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("majorWordsJson")).words);
    }

    getFullSentences() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("majorSentencesJson")).sentences);
    }

    getWelcomes() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("majorWelcomesJson")).welcomes);
    }

    getFarewells() {
        return this.swapPlaceholders(JSON.parse(GM_getResourceText("majorFarewellsJson")).farewells);
    }

    getTranslations() {
        return JSON.parse(GM_getResourceText("majorTranslationsJson")).translations;
    }

    getProbabilityOfAddingNewWord() {
        return 35;
    }

    getProbabilityOfAddingNewSentence() {
        return 30;
    }
}