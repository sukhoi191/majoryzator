/**
 * Enumeracja dla typu indeksu tłumaczenia.
 */
export const TranslationCapitalizationType = {
    UPPERCASE: 1,
    LOWERCASE: 2,
    TITLECASE: 3,
    UNKNOWN: 4,
    NOT_FOUND: 5
};