import { REGEX_LETTERS } from './Configuration.Functional';
import { Person } from './Person';

/**
 * Majoryzator stron internetowych.
 */
export class WebsiteMajorizator {
    /**
     * Tworzy nową instancję.
     * @param {Person} person Osoba.
     */
    constructor(person) {
        this.nodesToMajorize = [];
        this.person = person;
    }

    /**
     *  Sprawdza, czy dzieci podanego węzła mogą
     * zostać poddane edycji.
     * @param {Node} node Węzeł do sprawdzenia.
     */
    areNodeChildrenMajorizable(node) {
        let tagsToOmmit = [
            "SCRIPT",
            "STYLE"
        ];

        return node != null && !tagsToOmmit.includes(node.tagName) && node.nodeType == 1;
    }

    /**
     * Sprawdza, czy węzeł może zostać poddany modyfikacji.
     * @param {Node} node Węzeł do sprawdzenia.
     */
    isNodeEditable(node) {
        if (node == null || typeof node != "object") {
            return false;
        }

        if (node.data === undefined || node.data == null || node.data == "" || !new RegExp('[' + REGEX_LETTERS + ']+').test(node.data)) {
            return false;
        }

        if (node.nodeType === undefined || node.nodeType != 3) {
            return false;
        }

        if (node.childNodes === undefined || typeof node.childNodes != "object") {
            return false;
        }

        return true;
    }

    /**
     * Dokonuje rekursywnej majoryzacji podanego węzła ze strony internetowej.
     * @param {Node} node Węzeł, dla którego wykonujemy majoryzację.
     */
    findNodesToMajorize(node) {
        if (this.isNodeEditable(node)) {
            this.nodesToMajorize.push(node);
        }

        if (this.areNodeChildrenMajorizable(node)) {
            node.childNodes.forEach(function (child) {
                this.findNodesToMajorize(child);
            }.bind(this));
        }
    }

    /**
     * Majoryzuje znalezione węzły.
     * @param {Generator} generator Instanacja Generatora.
     * @param {string} conjunctionBeforeBlacklistJson Czarna lista słów, po których nie można wstawić spójnika.
     * @param {string} conjunctionAfterBlacklistJson Czarna lista słów, przed którymi nie można wstawić spójnika.
     */
    majorizeNodes(generator, conjunctionBeforeBlacklistJson, conjunctionAfterBlacklistJson) {
        for (let i = 0; i < this.nodesToMajorize.length; i++) {
            let node = this.nodesToMajorize[i];
            node.data = generator.generate(node.data, i == 0, i == this.nodesToMajorize.length - 1,
                conjunctionBeforeBlacklistJson,
                conjunctionAfterBlacklistJson);
        }
    }

    /**
     * Uruchamia majoryzację całej strony.
     */
    majorizeWholeWebsite(conjunctionBeforeBlacklistJson, conjunctionAfterBlacklistJson,
        knownAbbreviationsJson, tentativeEndsJson) {
        this.findNodesToMajorize(document.body);
        this.majorizeNodes(new Generator(this.person,
            knownAbbreviationsJson,
            tentativeEndsJson),
            conjunctionBeforeBlacklistJson,
            conjunctionAfterBlacklistJson);
    }
}