# Checklista wypuszczenia nowej wersji skryptu

1. W przypadku zmiany numeru major / minor - utworzenie milestone na GitLabie i podpięcie do niego wykonanych zadań.
2. Aktualizacja @version w headerze.
3. Aktualizacja version w package.json.
4. Commit ze zmianami do brancha `dev`.
5. Merge request do brancha `master`.
6. Akceptacja merge requesta (lub nie).
7. Build i wrzucenie nowej wersji na openuserjs.org.
8. Utworzenie taga.