# Majoryzator

Skrypt przerabiający treść stron internetowych na styl wypowiedzi Majora Wojciecha S. (bo RODO) oraz jego kompanów.

## Jak zacząć?

1. Zainstaluj wtyczkę Tampermonkey dla swojej przeglądarki (https://www.tampermonkey.net/).
2. Zainstaluj skrypt Majoryzator (https://openuserjs.org/scripts/sukhoi191/Majoryzator).
3. Upewnij się, że skrypt jest włączony w ustawieniach Tampermonkey.
4. Wejdź na stronę, którą chcesz przerobić.
5. W prawym dolnym rogu strony powinny pojawić się dyskretne przyciski umożliwiające uruchomienie skryptu.

## Budowanie

1. Ściągnij i zainstaluj node.js (https://nodejs.org/en/download/).
2. Ściągnij i zainstaluj Python (https://www.python.org/downloads/).
3. Uruchom plik build.py.

## Testy

W katalogu z projektem:

1. Uruchom polecenie `npm install` (jednorazowo).
2. Uruchom polecenie `npm run test`.

## Współpraca

Znalazłeś błąd w działaniu skryptu? Masz pomysł na nową funkcjonalność? A może chcesz pomóc w pisaniu kodu? Napisz do mnie na sukhoi191@protonmail.com.

Aktualnie dodawane funkcje i istniejące pomysły znajdziesz na https://gitlab.com/sukhoi191/majoryzator/-/boards.

Pamiętaj, aby za pomocą łapek w dół / w górę głosować na funkcje, które chcesz zobaczyć najpierw :)

## Autorzy

* **sukhoi191 (Bartłomiej Zieliński)** - pomysł, praca nad kodem, poprawki błędów.

## Licencja

Ten projekt objęty jest licencją MIT - jej treść jest dostępna w pliku LICENSE.md.

## Wersjonowanie

Projekt korzysta z wersjonowania na zasadach Semantic Versioning 2.0.0 (https://semver.org/).

## Podziękowania

- **keraj** - twórca skryptu wstawiającego losowo słowa na "k" do treści wyświetlanej strony. Ten skrypt posłużył jako inspiracja dla Majoryzatora.
- **http://kononowicz-suchodolski.cba.pl/ [dostęp 06.10.2019]** - część tekstów Kononowicza i Majora.
- **https://www.facebook.com/WojciechSuchodolskiCytaty** - duża część tekstów Majora.
- **https://www.facebook.com/Major-Cytaty-całe-te-387885275022447** - część tekstów Majora.
- **polaq (Wykop)** - sugestie dotyczące JS oraz przerobienia skryptu na userscript.
- **Wap30 (Wykop)** - sugestie dotyczące JS.
- **jakiekur. (bo RODO) (Discord)** - sugestie dotyczące tłumaczeń.
- **https://jsonschema.net/ [dostęp 19.10.2019]** - generowanie JSON schema na bazie istniejących plików JSON.